# Orbit Theme

This website is my implementation of Xiaoying Riley's [Orbit](http://themes.3rdwavemedia.com/) theme, which is free under the [Creative Commons Attribution 3.0 License](
https://creativecommons.org/licenses/by/3.0/).

### Gulp Tasks
Gulp is included to help with build tasks, like:

| Task name | Description | Command |
|-----------|-------------|---------|
| BundleJS  | Minify and concatenate Javascript files from the `JS_SRC` folder(s), turning them into a `mysite.min.js` file | `gulp bundleJS`
| BundleCSS | Minify and concatenate CSS files from the `CSS_SRC` folder(s), turning them into a `bundle.css` file | `gulp bundleCSS` |
| bundleHTML | Minify HTML files from the `HTML_SRC` folder(s) | `gulp bundleHTML` |

### NPM Scripts

NPM scripts were added as a quality-of-life feature. After running `npm install` you'll be able to run the following scripts.

| Script name | Description | Command |
|-----------|-------------|---------|
| Serve  | Run `/dist/` folder on a local Apache server on port  4201 | `npm serve`
| Build | Build `/dist/` folder | `npm build` |
| Deploy | Build the site, then deploy the `/dist/` folder on the Firebase project set on your `.firebaserc` file | `npm deploy` |