export const handleProfilePic = () => {
  const profilePicture = document.querySelector('.profile')
  const blockedCountries = ['CL', 'US'] // hide Picture in these countries

  fetch('https://ipinfo.io/geo')
    .then((response) => {
      return response.json()
    })
    .then((response) => {
      const setDisplay = (show) => {
        profilePicture.style.display = (show) ? 'block' : 'none'
      }

      if (response.status > 300)
        setDisplay(false)
      else {
        const blockCountry = blockedCountries.includes(response.country)
        setDisplay(blockCountry)
      }

    })
    .catch(console.error)
}
