import { handleProfilePic } from './profile-pic'
import { levelsHandler } from './levels'
import { analyticsHandler } from './analytics'

document.addEventListener('DOMContentLoaded', () => {
  handleProfilePic()
  levelsHandler()
  analyticsHandler()
})
