export const levelsHandler = () => {
  const levelBars = document.querySelectorAll('.level-bar-inner')

  const setLevels = (arr) => {
    Array.from(arr)
      .forEach((bar) => {
        const level = bar.getAttribute('data-level')
        console.log(level)
        bar.style.width = level
      })
  }
  setLevels(levelBars)
}
