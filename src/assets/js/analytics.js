export const analyticsHandler = () => {
  window.dataLayer = window.dataLayer || []

  function gtag() { dataLayer.push(arguments) }

  gtag('js', new Date())
  gtag('config', 'UA-113183171-1')
}
