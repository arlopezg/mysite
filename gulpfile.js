const gulp = require('gulp')
const concatJS = require('gulp-concat')
const minifyJS = require('gulp-minify')
const minifyCSS = require('gulp-clean-css')
const concatCSS = require('gulp-concat-css')
const babel = require('gulp-babel');
const minifyHTML = require('gulp-htmlmin')
const copy = require('gulp-copy')

const browserSync = require('browser-sync').create()

const target = { PATH: './dist/' }

const src = {
  HTML_SRC: './src/**/*.html',
  JS_SRC: './src/assets/js/*.js',
  CSS_SRC: [
    './src/assets/plugins/bootstrap/css/bootstrap.min.css',
    './src/assets/css/styles.css'
  ],
  OTHER: [
    './src/**/*.png',
    './src/**/*.pdf',
    './src/**/*.ico',
    './src/**/*.webp',
    './src/**/*.json',
    './src/**/*.txt'
  ]
}

gulp.task('default', ['scripts', 'styles', 'markup', 'workOthers'])

gulp.task('markup', function () {
  return gulp
    .src(src.HTML_SRC)
    .pipe(minifyHTML({
      'collapseWhitespace': true,
      'collapseBooleanAttributes': true,
      'removeComments': true,
      'removeEmptyAttributes': true,
      'removeRedundantAttributes': true
    }))
    .pipe(gulp.dest(target.PATH))
})

gulp.task('styles', function () {
  return gulp
    .src(src.CSS_SRC)
    .pipe(concatCSS('bundle.css'))
    .pipe(minifyCSS({ debug: true },
      (details) => {
        const original = details.stats.originalSize
        const minified = details.stats.minifiedSize
        console.log(`[Bundled] CSS: ${toKb(original)}kB to ${toKb(minified)}kB`)
      }))
    .pipe(gulp.dest(target.PATH))
})

gulp.task('scripts', function () {
  return gulp
    .src(src.JS_SRC)
    .pipe(concatJS('mysite.js'))
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(minifyJS({
      ext: { min: '.min.js' },
      compress: {
        sequences: true,// join consecutive statemets with the “comma operator”
        properties: true,// optimize property access: a["foo"] → a.foo
        dead_code: true,// discard unreachable code
        drop_debugger: true,// discard “debugger” statements
        conditionals: true,// optimize if-s and conditional expressions
        comparisons: true,// optimize comparisons
        evaluate: true,// evaluate constant expressions
        booleans: true,// optimize boolean expressions
        loops: true,// optimize loops
        unused: true,// drop unused variables/functions
        if_return: true,// optimize if-s followed by return/continue
        join_vars: true // join var declarations
      },
      noSource: true,
      exclude: ['tasks'],
      ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest(target.PATH))
})

gulp.task('workFonts', function () {
  gulp.src(src.FONTS)
    .pipe(copy(target.PATH, { prefix: 3 }))
})

gulp.task('workOthers', function () {
  return gulp
    .src(src.OTHER)
    .pipe(gulp.dest(target.PATH))
})

gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: 'dist/'
    },
  })
})

gulp.task('watch', ['browserSync', 'default'], () => {
  gulp.watch(
    [
      src.JS_SRC,
      src.CSS_SRC,
      src.HTML_SRC
    ],
    ['default', browserSync.reload]
  )
})

const toKb = (bytes) => {
  return (bytes / 1024).toFixed(0)
}
